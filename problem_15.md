---
layout: default
---

### Problem 15: Big Multiplication _(Answered)_

Output a multiplication between two numbers.
Input is positive large number from `0` to `10^20`.
Output must be written in regular form (`5600` not `5.6E3`).

### Case 1

#### Input
``` sh
100200
101
```

#### Output
```sh
10120200
```

### Case 2

#### Input
``` sh
10101
100200300400500600
```

#### Output
``` sh
1012123234345456560600
```

### Answer by _Emir Fakhri Muttaqiy_ from _Yosemite_

``` c
#include <stdio.h>
#include <string.h>

int main()
{
    int a[100],b[100];
    int ans[200]={0};
    int i,j,tmp;
    char s1[101],s2[101];
    scanf(" %s",s1);
    scanf(" %s",s2);
    int l1 = strlen(s1);
    int l2 = strlen(s2);
    for(i = l1-1,j=0;i>=0;i--,j++)
    {
        a[j] = s1[i]-'0';
    }
    for(i = l2-1,j=0;i>=0;i--,j++)
    {
        b[j] = s2[i]-'0';
    }
    for(i = 0;i < l2;i++)
    {
        for(j = 0;j < l1;j++)
        {
            ans[i+j] += b[i]*a[j];
        }
    }
    for(i = 0;i < l1+l2;i++)
    {
        tmp = ans[i]/10;
        ans[i] = ans[i]%10;
        ans[i+1] = ans[i+1] + tmp;
    }
    for(i = l1+l2; i>= 0;i--)
    {
        if(ans[i] > 0)
            break;
    }
    printf("Product : ");
    for(;i >= 0;i--)
    {
        printf("%d",ans[i]);
    }
    return 0;
}
```