---
layout: default
---

### Problem 12: Byte Converter _(Answered)_

Convert a decimal number to nearest `Base 2 Byte` notation (up to `tebibyte`).
a kibibyte (`KiB`) equals to 1024 byte (`B`), a mebibyte (`MiB`) equals to 1024 kibibyte (`KiB`), a gibibyte (`GiB`) equals to 1024 mebibyte (`MiB`), and a tebibyte (`TiB`) equals to 1024 gibibyte (`GiB`).

### Case 1

#### Input
``` sh
1020
```

#### Output
```sh
1020 B
```

### Case 2

#### Input
``` sh
58000
```

#### Output
``` sh
56 KiB
```

### Case 3

#### Input
``` sh
900000000
```

#### Output
``` sh
858 MiB
```

### Answer by _Abu Bakar BSA_ from _Cheetah_
``` c
#include<stdio.h>
int main() {
    long long int data,ukuran=0;
    scanf("%lld", &data);
    while(data>=1024){
        data/=1024;
        ukuran++;
    }
    printf("%d ", data);
    switch(ukuran) {
        case 0:
        printf(" B");
        break;
        case 1:
        printf("KiB");
        break;
        case 2:
        printf("MiB");
        break;
        case 3:
        printf("GB");
        break;
        case 4:
        printf("TB");
        break;
    }
    printf("\n");
}
```