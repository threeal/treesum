---
layout: default
---

### Problem 3: Youngster Encode _(Answered)_

For each alphabet in the sentence:
- `The First` and `the last` alphabets are uppercase.
- `i = 1`, `r = 2`, `e = 3`, `a = 4`, `s = 5`, `g = 6`, `j = 7`, `b = 8`, `o = 0`.

> **Hint!**: Iterate the string using `for`, change the alphabet to number with `switch`, and the first alphabet in the sentence or the alphabet before space is the first alphabet, while the last alphabet in the sentence or the alphabet after the space is the last alphabet.

### Case 1

#### Input
``` sh
namaku budi
```

#### Output
``` sh
N4m4kU 8ud1
```

### Case 2

#### Input
``` sh
aku suka membaca
```

#### Output
``` sh
4kU 5uk4 M3m84c4
```

### Answer by _Dion Andreas Solang_ from _Pangolin_

``` c
///////////////////////////////////
//NAMA    : DION ANDREAS SOLANG  //
//NRP     : 07211940000039       //
//KELOMPOK: PANGOLIN             //
///////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main()
{
    char setring[100][20];
    char encoder[]="48cd3f6h17klmn0pq25tuvwxyz";
    int indx=0,i,j;
    //bool flag=0;
    while(scanf("%s",&setring[indx])!=EOF)
        indx++;

    for(i=0;i<indx;i++){
        if(i!=0)printf(" ");
        for(j=0;j<strlen(setring[i]);j++){
            setring[i][j]=encoder[tolower(setring[i][j])-'a'];
            if(j==0)
                setring[i][j]=toupper(setring[i][j]);
            if(j==strlen(setring[i])-1)
                setring[i][j]=toupper(setring[i][j]);
        }
        printf("%s",setring[i]);
    }
    printf("\n");
    return 0;
}
```