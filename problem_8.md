---
layout: default
---

### Problem 8: Frequent Consonants _(Answered)_

Output all consonants that appear frequently.
Output in uppercase, from `A` to `Z`

### Case 1

#### Input
``` sh
Mobil Tua Bangka
```

#### Output
```sh
B
```

### Case 2

#### Input
``` sh
Menuju Tak Terbatas dan Melampauinya
```

#### Output
``` sh
M N T
```

### Answer by _Theodorus Wensan Februanto_ from _Pangolin_
``` c
#include <stdio.h>
#include <string.h>

//PROBLEM 8
//COPYRIGHT BY THEODORUS WENSAN FEBRUANTO
//07211940000025
int check_vowel(char);
void lower_string(char []);

int main()
{
    char string[100], d[26], e[26], t[100];
    int i, c = 0, count[26] = {0}, x, max = 0, q, w = 0;

    gets(string); //Input string
    lower_string(string);

    for (q = 0; string[q]!='\0'; q++) {
        if(check_vowel(string[q]) == 0) { //Kalau bukan huruf vokal, atau vowel
            t[w] = string[q];
            w++;
        }
    }

    t[w] = '\0';
    strcpy(string, t);

    while (string[c] != '\0') {
        if (string[c] >= 'a' && string[c] <= 'z') {
            x = string[c] - 'a';
            count[x]++;
        }
        c++;
    }

    for (c=0; c<26; c++) {
        if (count[c] > 0) {
            d[c] = c + 'a';
            if (max < count[c]) {
                max = count[c];
            }
        }
    }

    for (i=0; i<strlen(count); i++) {
        if (max < count[i]) {
            max = count[i];
        }
    }

    for (int k=0; k<26; k++) {
        if (count[k] == max) {
            e[k] = d[k] - 32;
            printf("%c ", e[k]);
        }
    }

    return 0;
}

void lower_string(char s[]) {
    int g = 0;

    while (s[g] != '\0') {
        if (s[g] >= 'A' && s[g] <= 'Z') {
            s[g] = s[g] + 32;
        }
        g++;
    }
}

int check_vowel(char ch)
{
    if (ch == 'a' || ch == 'e' || ch == 'i' || ch =='o' || ch == 'u') {
        return 1;
    } else {
        return 0;
    }
}
```