---
layout: default
---

### Problem 13: Big Float _(Answered)_

Output addition of 2 float numbers, each number has maximum 16 digits of accuracy.
Output must be written in regular form (`0.0078` not `7.8E-3`).

### Case 1

#### Input
``` sh
1000.50
20.405
```

#### Output
```sh
1020.905
```

### Case 2

#### Input
``` sh
100200300400.500
100.200300400500
```

#### Output
``` sh
100200300500.700300400500
```

### Answer by _I Gusti Komang Agung Wiguna_ from _Cheetah_
``` c
#include<stdio.h>
#include<math.h>
#include<string.h>
int digit(long long int bil) {
    int _digit = 1;
    while(bil>1) {
        bil/=10;
        _digit++;
    }
    return _digit;
}
int main() {
    char a[1000], b[1000];
    long long int a1 = 0, a2 = 0, b1 = 0, b2 = 0;
    scanf("%s %s", a, b);
    int titik1 = 0;
    while(a[titik1]!='.')
        titik1++;
    for(int i = 0; i<titik1; i++)
        a1+=(a[i]-'0')*pow(10, titik1-i-1);
    int titik2 = 0;
    while(b[titik2]!='.')
        titik2++;
    for(int i = 0; i<titik2; i++)
        b1+=(b[i]-'0')*pow(10, titik2-i-1);
    long long int depan = a1 + b1;
    int x = strlen(a)-titik1;
    int y = strlen(b)-titik2;
    int max;
    if(y>=x) {
        max = y;
    } else {
        max = x;
    }
    for(int i= titik1+1; i<titik1+x; i++) {
        a2+=(a[i]-'0')*pow(10, titik1+max-i-1);
    }
    for(int i= titik2+1; i<titik2+y; i++) {
        b2+=(b[i]-'0')*pow(10, titik2+max-i-1);
    }
    long long int belakang = a2+b2;
    max--;
    if(digit(belakang)>max) {
        int selisih=belakang/pow(10, digit(belakang)-1);
        depan+=selisih;
        belakang-=selisih*pow(10, digit(belakang)-1);
    }
    printf("%lld.%lld ", depan, belakang);
}
```