---
layout: default
---

### Problem 9: Big XOR _(Answered)_

Calculate XOR operation of two binary number (up to 32-bit).
Both numbers have the same length.

### Case 1

#### Input
``` sh
10011
01010
```

#### Output
```sh
11001
```

### Case 2

#### Input
``` sh
1010001001011100111110000
1010100101010101110101010
```

#### Output
``` sh
0000101100001001001011010
```

### Answer by _Felix Titus Setiawan_ from _Pangolin_

``` c
#include <stdio.h>
#include <ctype.h>
#include <string.h>

int main(){
	char a[32],b[32];
	int i;
	scanf("%s",&a);
	scanf("%s",&b);
	for(i=0;i<strlen(a);i++){
		if(a[i]!=b[i])
			printf("1");
		else
			printf("0");
	}
	printf("\n");




	return 0;
}
```