---
layout: default
---

### Problem 2: Number To Word _(Answered)_

Input from `0` - `99`.

### Case 1

#### Input
```sh
11
```

#### Output
``` sh
sebelas
```

### Case 2

#### Input
```sh
89
```

#### Output
``` sh
delapan puluh sembilan
```

### Answer by _Nicholas Chang_ from _Zesty_
``` c
#include <stdio.h>
#include <conio.h>

void terbilang (int x);

int main()
{
int angka;

printf("%s","Masukan angka dari satu sampai sembilan puluh sembilan : ");
scanf("%d",&angka);
terbilang(angka);
}

void terbilang(int x){
//algoritma
if(x==1){
printf("Satu ");
}else if(x==2){
printf("Dua ");
}else if(x==3){
printf("Tiga ");
}else if(x==4){
printf("Empat ");
}else if(x==5){
printf("Lima ");
}else if(x==6){
printf("Enam ");
}else if(x==7){
printf("Tujuh ");
}else if(x==8){
printf("Delapan ");
}else if(x==9){
printf("Sembilan ");
}else if(x==10){
printf("Sepuluh ");
}else if(x==11){
printf("Sebelas ");
}else if(x>=12&&x<=19){
terbilang(x%10);
printf("Belas ");
}else if(x>=20&&x<=99){
terbilang(x/10);
printf("Puluh ");
terbilang(x%10);
return 0;
}}
```