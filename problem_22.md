---
layout: default
---

### Problem 22: Uncommon Word Sort

Sort series of words.
But instead of sort it in regular order _(`A`, `B`, `C`, .. `Y`, `Z`)_, sort it in the following order: `I`, `E`, `N`, `J`, `O`, `K`, `P`, `Y`, `Z`, `A`, `F`, `B`, `W`, `X`, `Q`, `R`, `D`, `V`, `M`, `G`, `U`, `T`, `C`, `H`, `L`, `S`.
So `C` is larger than `X` and `Race` is larger than `Rocket`.

### Case 1

#### Input
``` sh
Roll Horse Rocket Holy Human
```

#### Output
```sh
Rocket Roll Horse Holy Human
```

### Case 2

#### Input
``` sh
Jaguar Gorilla Grenade Godzilla Jazz
```

#### Output
``` sh
Jazz Jaguar Gorilla Godzilla Grenade
```