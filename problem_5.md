---
layout: default
---

### Problem 5: Youngster Decode _(Answered)_

For each alphabet in the sentence:
- Lowercased output.
- `1 = i`, `2 = r`, `3 = e`, `4 = a`, `5 = s`, `6 = g`, `7 = j`, `8 = b`, `0 = o`.

> **Hint!**: Iterate the string using `for`, change the alphabet to number with `switch`, and to lowercased the output you may use `tolower()` function from `cctype` library, or mind your own logic.

### Case 1

#### Input
``` sh
D1m4s an4K y4Ng b4Ik
```

#### Output
```sh
dimas anak yang baik
```

### Answer by _Adritia Alfiana Merdila_ from _Mavericks_
``` c
#include <ctype.h>
#include <stdio.h>

int main()
{
	char a;
	int b,c;
	c=100;
	for(b=0;b<c;b++)
	{
		a=getchar();
		switch(a)
		{
			case '1':
				printf("i");
				goto portal;
			case '2':
				printf("r");
				goto portal;
			case '3':
				printf("e");
				goto portal;
			case '4':
				printf("a");
				goto portal;
			case '5':
				printf("s");
				goto portal;
			case '6':
				printf("g");
				goto portal;
			case '7':
				printf("j");
				goto portal;
			case '8':
				printf("b");
				goto portal;
			case '0':
				printf("o");
				goto portal;
			}
		printf("%c",tolower(a));
		portal:
		printf("");
	}
    return 0;
}
```