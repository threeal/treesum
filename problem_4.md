---
layout: default
---

### Problem 4: Word To Number _(Answered)_

Input `nol`, `satu`, ... , `sembilan puluh sembilan`.
For invalid input, output `-1`.

### Case 1

#### Input
```sh
lima puluh
```

#### Output
``` sh
50
```

### Case 2

#### Input
```sh
sengbilan
```

#### Output
``` sh
-1
```

### Answer by _Aaron Christopher Tanhar_ from _Zesty_

``` c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    char angka[50];
    gets(angka);

    char *kata2 = "NULL";
    char *kata3 = "NULL";

    char *kata1 = strtok(angka, " ");
    kata2 = strtok(NULL, " ");
    kata3 = strtok(NULL, "*\n");

    int num[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
    int a;
    int b;

    for(int i = 0;i<=2;i++){
        if(i == 0){
            if(strcmp(kata1, "nol") == 0){
                a = num[0];
            }
            else if(strcmp(kata1, "satu") == 0){
                a = num[1];
            }
            else if(strcmp(kata1, "dua") == 0){
                a = num[2];
            }
            else if(strcmp(kata1, "tiga") == 0){
                a = num[3];
            }
            else if(strcmp(kata1, "empat") == 0){
                a = num[4];
            }
            else if(strcmp(kata1, "lima") == 0){
                a = num[5];
            }
            else if(strcmp(kata1, "enam") == 0){
                a = num[6];
            }
            else if(strcmp(kata1, "tujuh") == 0){
                a = num[7];
            }
            else if(strcmp(kata1, "delapan") == 0){
                a = num[8];
            }
            else if(strcmp(kata1, "sembilan") == 0){
                a = num[9];
            }
            else if(strcmp(kata1, "sepuluh") == 0){
                a = num[10];
            }
            else if(strcmp(kata1, "sebelas") == 0){
                a = num[11];
            }
            else{
                a = -1;
                printf("%d", a);
                break;
            }
        }

        else if(i == 1 && kata2 != NULL){
            if(strcmp(kata2, "belas") == 0){
                b = a;
                a = 1;
            }
            else if(strcmp(kata2, "puluh") == 0 && kata3 == NULL){
                b = 0;
            }
            else if(strcmp(kata2, "puluh") == 0){
                continue;
            }
            else{
                a = -1;
                printf("%d", a);
                break;
            }
        }

        else if(i == 2 && kata3 != NULL){
            if(strcmp(kata3, "satu") == 0){
                b = num[1];
            }
            else if(strcmp(kata3, "dua") == 0){
                b = num[2];
            }
            else if(strcmp(kata3, "tiga") == 0){
                b = num[3];
            }
            else if(strcmp(kata3, "empat") == 0){
                b = num[4];
            }
            else if(strcmp(kata3, "lima") == 0){
                b = num[5];
            }
            else if(strcmp(kata3, "enam") == 0){
                b = num[6];
            }
            else if(strcmp(kata3, "tujuh") == 0){
                b = num[7];
            }
            else if(strcmp(kata3, "delapan") == 0){
                b = num[8];
            }
            else if(strcmp(kata3, "sembilan") == 0){
                b = num[9];
            }
            else{
                a = -1;
                printf("%d", a);
                break;
            }
        }
    }

    if(kata2 == NULL && kata3 == NULL && a != -1){
        printf("%d", a);
    }
    else if(a != -1){
        printf("%d%d", a, b);
    }


    return 0;
}
```