---
layout: default
---

### Problem 14: Elementary Word Math _(Answered)_

Input 2 `nol`, `satu`, ... , `sepuluh` words.
and either `ditambah` or `dikali` operator.
Output the result in sentence.
For invalid input, output `invalid`.

### Case 1

#### Input
```sh
lima dikali sepuluh
```

#### Output
``` sh
lima puluh
```

### Case 2

#### Input
```sh
tujuh ditambah sembilan
```

#### Output
``` sh
enam belas
```

### Case 3

#### Input
```sh
nol dkli sengbilan
```

#### Output
``` sh
invalid
```

### Answer by _Renaka Agusta_ from _Cheetah_
``` c
#include<stdio.h>
#include<string.h>
int main() {
    char satuan[11][10] = {"nol","satu", "dua","tiga", "empat", "lima", "enam", "tujuh","delapan","sembilan","sepuluh"};
    char opr[10];
    char bil1[10], bil2[10];
    scanf("%s", bil1);
    scanf("%s", opr);
    scanf("%s", bil2);
    int i = 0, j = 0;
    int hasil = -1;
    for(;i<11; i++) if(strcmp(satuan[i], bil1)==0) break;
    for(;j<11; j++) if(strcmp(satuan[j], bil2)==0) break;
    if((i<11)&&(j<11)) {
        if(strcmp(opr, "dikali")==0) {
            hasil = i*j;
        } else if(strcmp(opr, "ditambah")==0) {
            hasil = i+j;
        } else {
            printf("invalid");
        }
    } else {
        printf("invalid");
    }
    if(hasil!=-1) {
        if(hasil<11) {
            printf("%s", satuan[hasil]);
        } else if(hasil==11) {
            printf("sebelas");
        } else if(hasil<20) {
            printf("%s belas", satuan[hasil-10]);
        } else if(hasil<100) {
            printf("%s puluh ", satuan[hasil/10]);
            if(hasil%10!=0) printf("%s", satuan[hasil%10]);
        } else if(hasil==100) {
            printf("seratus");
        }
    }
    printf("\n");
}
```