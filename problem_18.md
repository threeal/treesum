---
layout: default
---

### Problem 18: Algebra Math _(Answered)_

Input an algebra equation, restricted to only addition(`+`) and substraction(`-`).
Each equation will always had an equation mark (`=`) and an `x`.
Every number and operator will be separated with a single space (` `).
Output the value of `x` based on the provided equation.

### Case 1

#### Input
``` sh
5 + x = 8
```

#### Output
```sh
3
```

### Case 2

#### Input
``` sh
32 - 11 = 10 + x
```

#### Output
``` sh
11
```

### Answer by _Lubna Khalidah_ from _Elcaptican_

``` c
#include <stdio.h>
#include <string.h>
int isNumber(char z)
{
	if('0' <= z && z <= '9') return 1;
	return 0;
}

int main(void) {
	char equation[128];
	scanf("%[^\n]s", equation);
	int i = 0;
	int left_total = 0, right_total = 0, phrase = 0, x_status = 1, x_phrase = 0;
	int temporarly_number = 0, temporarly_status = 1;
	for(i = 0; i < strlen(equation); i++)
	{
		if(isNumber(equation[i]))
		{
			int number = equation[i]-'0';
			temporarly_number = temporarly_number*10 + number;
		}
		else if(equation[i] == '+')
		{
			if(phrase == 0)
			{
				left_total = left_total + temporarly_status*temporarly_number;
				temporarly_status = 1;
				temporarly_number = 0;
			}
			else if(phrase == 1)
			{
				right_total = right_total + temporarly_status*temporarly_number;
				temporarly_status = 1;
				temporarly_number = 0;
			}
		}
		else if(equation[i] == '-')
		{
			if(phrase == 0)
			{
				left_total = left_total + temporarly_status*temporarly_number;
				temporarly_status = -1;
				temporarly_number = 0;
			}
			else if(phrase == 1)
			{
				right_total = right_total + temporarly_status*temporarly_number;
				temporarly_status = -1;
				temporarly_number = 0;
			}
		}
		else if(equation[i] == '=')
		{
			left_total = left_total + temporarly_status*temporarly_number;
			temporarly_status = 1;
			temporarly_number = 0;
			phrase = 1;
		}
		else if(equation[i] == 'x')
		{
			x_status = temporarly_status;
			x_phrase = phrase;
			temporarly_number = 0;
			temporarly_status = 1;
		}
	}
	//checking if right_total is not zero
	right_total = right_total + temporarly_status*temporarly_number;
	if(x_status == -1)
	{
		if(x_phrase == 0) x_phrase = 1;
		else if(x_phrase == 1) x_phrase = 0;
	}
	if(x_phrase == 0) //x is on the left side
	{
		printf("%d\n", right_total-left_total);
	}
	else if(x_phrase == 1)
	{
		printf("%d\n", left_total-right_total);
	}
	return 0;
}

```