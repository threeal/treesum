---
layout: default
---

### Problem 11: Uncommon Sort _(Answered)_

Sort series of numbers.
But instead of sort it in regular order _(`0`, `1`, `2`, .. `8`, `9`)_, sort it in the following order: `0`, `1`, `3`, `2`, `4`, `9`, `8`, `6`, `7`, `5`.
So `5` is larger than `9` and `71` is larger than `94`.

### Case 1

#### Input
``` sh
8 3 6 7 12
```

#### Output
```sh
3 8 6 7 12
```

### Case 2

#### Input
``` sh
12 56 32 48 90 102 193 167 28 53 78
```

#### Output
``` sh
12 32 28 48 90 78 53 56 102 193 167
```

### Answer by _Joseph Saido Gabriel_ from _Elcaptican_
``` c++
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int get_second(int a){
    a=a%100;
    while(a>10){
        a=a/10;
    }
    return a;

}

int get_first(int a){
    while(a > 10){
        a = a/10;
    }
    return a;
}

int main()
{
    char input[100];
    scanf("%[^\n]s", &input);
    int num[100];

    char *get_num;
    get_num = strtok(input, " ");
    int n = atoi(get_num);
    num[0] = n;

    int i = 1;

    while(get_num != NULL){
        get_num = strtok(NULL, " ");
        if(get_num != NULL){
          n = atoi(get_num);
        }
        else{
          break;
        }
        num[i] = n;
        i++;
    }

    int order[11] = {0, 1, 3, 2, 4, 9, 8, 6, 7, 5};

    int puluhan[100];
    for(int j = 0;j<11;j++){
        for(int k = 0;k<i;k++){
            if(num[k] == order[j]){
                printf("%d ", num[k]);
            }
        }
    }

    int puluhan_sorted[100][100];
    int count[10] = {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0};

    for(int j = 0;j<11;j++){
        for(int k = 0;k<i;k++){
            if(num[k] >= 10 && num[k] < 100){
                int first = num[k]/10;
                if(first == order[j] && num[k]%10!=0){
                    puluhan_sorted[j][count[j]] = num[k];
                    count[j]++;
                }
                else if(num[k]%10 == 0 && num[k]/10 == order[j]){
                    puluhan_sorted[j][count[j]] = num[k];
                    count[j]++;
                }
            }
        }
    }

    for(int i = 0;i<11;i++){
        for(int j = 0;j<11;j++){
            for(int k = 0;k<count[i];k++){
                int last = puluhan_sorted[i][k]%10;

                if(last == order[j] && last % 10 != 0){
                    printf("%d ", puluhan_sorted[i][k]);
                }
                if(last == order[j] && last % 10 == 0){
                    printf("%d ", puluhan_sorted[i][k]);
                    puluhan_sorted[i][k] = -1;
                }
            }
        }
    }

    int ratusan_sorted[100][100];
    int count2[10] = {0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0};
    for(int j = 0;j<11;j++){
        for(int k = 0;k<i;k++){
            if(num[k] >= 100){
                int first = num[k];
                while(first >= 10){
                    first = first / 10;
                }

                if(first == order[j]){
                    ratusan_sorted[j][count2[j]] = num[k];
                    count2[j]++;
                }
            }
        }
    }

    for(int i = 0;i<11;i++){
        for(int j = 0;j<11;j++){
            for(int k = 0;k<count2[i];k++){
                int second = get_second(ratusan_sorted[i][k]);
                if(second == order[j] && get_second(ratusan_sorted[i][k]) != 0){
                    printf("%d ", ratusan_sorted[i][k]);
                }
                if(second == order[j] && get_second(ratusan_sorted[i][k]) == 0){
                    printf("%d ", ratusan_sorted[i][k]);
                    ratusan_sorted[i][k] = -1;
                }
            }
        }
    }

    return 0;
}
```