---
layout: default
---

### Problem 21: Frequent Syllable

Output all Syllable that appear frequently.
Output in lowercase, separated by spaces ` `.

### Case 1

#### Input
``` sh
Mobil mobilan
```

#### Output
```sh
mo
```

### Case 2

#### Input
``` sh
Makan makanan di kantin
```

#### Output
``` sh
ma kan
`````