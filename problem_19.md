---
layout: default
---

### Problem 19: 3D Sphere _(Answered)_

Output maximum diameter of a sphere in the center coordinate before colliding with other spheres in 3d space.
`First input` indicates number of points.
`The next inputs` indicate `x`, `y`, `z`, and `d` for position and diametre of each sphere.

### Case 1

#### Input
``` sh
3
0 10 5 1
6 8 0 2
12 0 9 3
```

#### Output
```sh
18
```

### Case 2

#### Input
``` sh
7
12 43 0 8
21 13 56 4
0 21 28 12
8 41 21 1
34 56 23 2
12 100 12 3
0 56 89 10
```

#### Output
``` sh
58
```

### Answer by Nathanael Hutama Harsono_ from _Elcaptican_
```c
#include <stdio.h>
#include <math.h>
int main()
{
int a;
scanf ("%d", &a);
int x,y,z,d,sementara[a],i,r,c,hasil;
for (i=0;i<a;i++){
scanf("%d %d %d %d", &x,&y,&z,&d);
	c=sqrt(x*x+y*y+z*z);
	r=d/2;
	sementara[i]= 2*(c-r);
	}

hasil=sementara[0];
for (i=1;i<a;i++){
	if (hasil>sementara[i]){
		hasil=sementara[i];
	}
}
printf("%d", hasil);
}
```