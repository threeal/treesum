---
layout: default
---

Each person in the team chooses one different unanswered problem, work on it, and then email the source code to `alfi.maulana.f@gmail.com`.
After every person in the team emailed the source code, contact me for further information about the assistance date and locaton.
If there are difficulties while working on the answer, feel free to ask the b201 lab's assistants for helps.

Problem list:
- [**Problem 1: Syllable**](problem_1) _(**hint!**)_
- [~~Problem 2: Number To Word~~](problem_2) _(Answered)_
- [~~Problem 3: Youngster Encode~~](problem_3) _(Answered)_
- [~~Problem 4: Word To Number~~](problem_4) _(Answered)_
- [~~Problem 5: Youngster Decode~~](problem_5) _(Answered)_
- [~~Problem 6: Big Double~~](problem_6) _(Answered)_
- [~~Problem 7: Elementary Math~~](problem_7) _(Answered)_
- [~~Problem 8: Frequent Consonants~~](problem_8) _(Answered)_
- [~~Problem 9: Big XOR~~](problem_9) _(Answered)_
- [~~Problem 10: 3D Milage~~](problem_10) _(Answered)_
- [~~Problem 11: Uncommon Sort~~](problem_11) _(Answered)_
- [~~Problem 12: Byte Converter~~](problem_12) _(Answered)_
- [~~Problem 13: Big Float~~](problem_13) _(Answered)_
- [~~Problem 14: Elementary Word Math~~](problem_14) _(Answered)_
- [~~Problem 15: Big Multiplication~~](problem_15) _(Answered)_
- [~~Problem 16: Metre Converter~~](problem_16) _(Answered)_
- [~~Problem 17: Vertical Square Pattern~~](problem_17) _(Answered)_
- [~~Problem 18: Algebra Math~~](problem_18) _(Answered)_
- [~~Problem 19: 3D Sphere~~](problem_19) _(Answered)_
- [~~Problem 20: Spartan Converter~~](problem_20) _(Answered)_
- [**Problem 21: Frequent Syllable**](problem_21)
- [**Problem 22: Uncommon Word Sort**](problem_22)
- **Problem 23: Negative Math** _(Soon)_
- **Problem 24: Big Division** _(Soon)_
- **Problem 25: Rotational Square Pattern** _(Soon)_