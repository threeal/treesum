---
layout: default
---

### Problem 20: Spartan Converter _(Answered)_

Convert input to Spartan numerals, it works just like Roman numerals where `O` equals to `1`, `Y` equals to `5`, `Z` equals to `10`, `W` equals to `50`, `I` equals to `100`, `S` equals to `500`, `H` equals to `1000`, `R` equals to `5000`, `K` equals to `10000`, `E` equals to `50000`, `V` equals to `100000`, `X` equals to `500000`, `W` equals to `1000000`.

### Case 1

#### Input
``` sh
32
```

#### Output
```sh
ZZZOO
```

### Case 2

#### Input
``` sh
784900
```

#### Output
``` sh
XVVEKKKHRIH
```

### Answer by _I Putu Haris Setiadi Ekatama_ from _Sierra_
```c
#include <stdio.h>

int main()
 {
 	long long n;
 	char x;

 	scanf ("%llu",&n);
 	while(n!=0){
		if (n >= 1000000)
		{
	 		printf("W");
	 		n-=1000000;
		}
		else if(n>=500000)
		{
			if(n>=900000)
			{
				printf("VW");
				n-=900000;
			}
			else
			{
				printf("X");
				n-=500000;
			}
		}
		else if (n>=100000)
		{
			if(n>=400000)
			{
				printf("VX");
				n-=400000;
			}
			else
			{
				printf("V");
				n-=100000;
			}
		}
		else if (n>=50000)
		{
			if(n>=90000)
			{
				printf("KV");
				n-=90000;
			}
			else
			{
				printf("E");
				n-=50000;
			}
		}
		else if (n>=10000)
		{
			if(n>=40000)
			{
				printf("KE");
				n-=40000;
			}
			else
			{
				printf("K");
				n-=10000;
			}
		}
		else if(n>=5000)
		{
			if(n>=9000)
			{
				printf("HK");
				n-=9000;
			}
			else
			{
				printf("R");
				n-=1000;
			}
		}
		else if(n>=1000)
		{
			if(n>=4000)
			{
				printf("HR");
				n-=4000;
			}
			else
			{
				printf("H");
				n-=1000;
			}
		}
		else if(n>=500)
		{
			if(n>=900)
			{
				printf("IH");
				n-=900;
			}
			else
			{
				printf("S");
				n-=500;
			}
		}
		else if(n>=100)
		{
			if(n>=400)
			{
				printf("IS");
				n-=400;
			}
			else
			{
				printf("I");
				n-=100;
			}
		}
		else if(n>=50)
		{
			if(n>=90)
			{
				printf("ZI");
				n-=90;
			}
			else
			{
				printf("W");
				n-=50;
			}
		}
		else if(n>=10)
		{
			if(n>=40)
			{
				printf("ZW");
				n-=40;
			}
			else
			{
				printf("Z");
				n-=10;
			}
		}
		else if(n>=5)
		{
			if(n>=9)
			{
				printf("OZ");
				n-=9;
			}
			else
			{
				printf("Y");
				n-=5;
			}
		}
		else if(n>=1)
		{
			if(n>=4)
			{
				printf("OY");
				n-=4;
			}
			else
			{
				printf("O");
				n-=1;
			}
		}
	}
 }
```