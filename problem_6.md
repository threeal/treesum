---
layout: default
---

### Problem 6: Big Double _(Answered)_

Output `the double` of input number (2x).
Input is positive large number from `0` to `10^30`.
Output must be written in regular form (`5600` not `5.6E3`).

### Case 1

#### Input
``` sh
300240
```

#### Output
```sh
600480
```

### Case 2

#### Input
``` sh
100200300400500600700800900
```

#### Output
``` sh
200400600801001201401601800
```

### Answer by _Chesia Grace Natalia_ from _Pangolin_

``` c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(){
	char BigNum[30];
	int curry[31];
	int i,a,length;
	memset(curry,0,sizeof curry);
	gets(BigNum);
	length=strlen(BigNum);
	for(i=0;i<length;i++)
		BigNum[i]=BigNum[i]-'0';
	for(i=length-1;i>=0;i--){
		a=BigNum[i];
		a*=2;
		a+=curry[i+1];
		BigNum[i]=a%10;
		curry[i]+=a/10;
	}
	if(curry[0]!=0)
		printf("%d",curry[0]);
	for(i=0;i<length;i++)
		printf("%c",BigNum[i]+'0');

}
```