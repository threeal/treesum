---
layout: default
---

### Problem 16: Metre Converter _(Answered)_

Convert either submultiples of metre or multiples of metre to metre (`m`).
Input is float number from `0.001` to `1000.0` followed by either submultiples or multiples of metre symbol from picometre (`pm`), nanometre (`nm`), micrometre (`um`), millimetre (`mm`), centimetre (`cm`), decimetre (`dm`), metre (`m`), decametre (`dam`), hectometre (`hm`), kilometre (`km`), megametre (`Mm`), gigametre (`Gm`) to terametre (`Tm`).
Output must be written in regular form (`0.0078` not `7.8E-3`).
Output `0 m` for invalid symbol.

### Case 1

#### Input
``` sh
5 km
```

#### Output
```sh
5000 m
```

### Case 2

#### Input
``` sh
56 nm
```

#### Output
``` sh
0.000000056 m
```

### Case 3

#### Input
``` sh
89 xm
```

#### Output
``` sh
0 m
```

### Answer by _Intro Brilliant Muhammad_ from _Cheetah_
``` c
#include<stdio.h>
#include<string.h>
int main(){
    double angka, hasil;
    char satuan[2];

    printf("Masukkan input : \n");
    scanf("%lf %s", &angka, satuan);

    if(strcmp(satuan, "pm")==0){
        hasil = angka * 1000;
        printf("%.12lf %s = %.12lf m", angka, satuan, hasil);
    }
    else if (strcmp(satuan, "nm")==0){
        hasil = angka * 0.000000001;
        printf("%.9f %s = %.9lf m", angka, satuan, hasil);
    }
    else if (strcmp(satuan, "um")==0){
        hasil = angka * 0.000001;
        printf("%.6f %s = %.6lf m", angka, satuan, hasil);
    }
    else if (strcmp(satuan, "mm")==0){
        hasil = angka * 0.001;
        printf("%.3lf %s = %.3lf m", angka, satuan, hasil);
    }
    else if (strcmp(satuan, "cm")==0){
        hasil = angka * 0.01;
        printf("%.2lf %s = %.2lf m", angka, satuan, hasil);
    }
    else if (strcmp(satuan, "dm")==0){
        hasil = angka * 0.1;
        printf("%.1lf %s = %.1lf m", angka, satuan, hasil);
    }
    else if (strcmp(satuan, "m")==0){
        hasil = angka * 1;
        printf("%.0lf %s = %.0lf m", angka, satuan, hasil);
    }
    else if (strcmp(satuan, "dam")==0){
        hasil = angka * 10;
        printf("%.0f %s = %.0lf m", angka, satuan, hasil);
    }
    else if (strcmp(satuan, "hm")==0){
        hasil = angka * 100;
        printf("%.0f %s = %.0lf m", angka, satuan, hasil);
    }
    else if (strcmp(satuan, "km")==0){
        hasil = angka * 1000;
        printf("%.0f %s = %.0lf m", angka, satuan, hasil);
    }
    else if (strcmp(satuan, "Mm")==0){
        hasil = angka * 1000000;
        printf("%.0f %s = %.0lf m", angka, satuan, hasil);
    }
    else if (strcmp(satuan, "Gm")==0){
        hasil = angka * 1000000000;
        printf("%.0f %s = %.0lf m", angka, satuan, hasil);
    }
    else if (strcmp(satuan, "Tm")==0){
        hasil = angka * 1000000000000;
        printf("%.0f %s = %.0lf m", angka, satuan, hasil);
    }
    else {
        printf("0 m");
    }

}
```