---
layout: default
---

### Problem 17: Vertical Square Pattern _(Answered)_

Create a square of number with size of `N` x `N` with the following pattern.
`1` < `N` < `50`.


### Case 1

#### Input
``` sh
3
```

#### Output
```sh
1 6 7
2 5 8
3 4 9
```

### Case 2

#### Input
``` sh
6
```

#### Output
``` sh
01 12 13 24 25 36
02 11 14 23 26 35
03 10 15 22 27 34
04 09 16 21 28 33
05 08 17 20 29 32
06 07 18 19 30 31
```

### Answer by _Galih Sukmamukti Hidayatullah_ from _Sierra_

``` c
#include <stdio.h>
#include <stdlib.h>

int main()
{
    int N;
    printf("Masukan nilai N, dimana 1<N<50 : ");
    scanf("%i",&N);
    if(N>1&&N<50)
    {
        int i,a[N],q;
        i=0;
        q=1;
        a[i-1]=0;
        while(i<N)
        {
            a[i]=a[i-1]+1;
            a[i+1]=N*(q*2);
            i=i+2;
            q++;
        }
        for(q=0;q<N;q++)
        {
            int w;
            for(w=0;w<N;w++)
            {
                if(N<4)
                {
                    printf("%i ",a[w]);
                }
                else if(N>3&&N<10)
                {
                    printf("%02i ",a[w]);
                }
                else if(N>9&&N<32)
                {
                    printf("%03i ",a[w]);
                }
                else if(N>31)
                {
                    printf("%04i ",a[w]);
                }
            }
            int g,e;
            for(i=0;i<N;i++)
            {
                g=i/2;
                g=g*2;
                e=i-g;
                if(e==0)
                {
                    a[i]=a[i]+1;
                }
                else if(e==1)
                {
                    a[i]=a[i]-1;
                }
            }
            printf("\n");
        }
    }
    else
    return 0;
}
```