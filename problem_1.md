---
layout: default
---

### Problem 1: Syllable

Split each syllable in the sentences using `-`.

> **Hint!**: Iterate the string using `for`, and if a vowel found, follow these rule:
> - If there is 1 or less consonant before the vowel, output all of it.
> - If there are 2 consonants before the vowel, output the last. Else, if it's `ny` or `ng` combination, output both.
> - If there are 3 consonants before the vowel, output the last.
> - If there are 4 consonants before the vowel, output 2 of the lasts.
> - Output the vowel.
> - If there are 2 consonants after the vowel, output the first. Else, if it's `ny` or `ng` combination, don't output.
> - If there are 3 or more consonants after the vowel, output 2 of the lasts.
> - If there is a word after the last consonant, output `-`.


### Case 1

#### Input
``` sh
aku punya anjing kecil
```

#### Output
``` sh
a-ku pu-nya an-jing ke-cil
```

### Case 2

#### Input
``` sh
suka berlarilari
```

#### Output
``` sh
su-ka ber-la-ri-la-ri
```