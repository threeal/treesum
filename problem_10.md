---
layout: default
---

### Problem 10: 3D Milage _(Answered)_

Calculate milage from series of 3-dimensional points.
`First input` indicates number of points.
`The next inputs` indicate `x`, `y`, and `z` position of each point.

### Case 1

#### Input
``` sh
3
0 0 0
2 0 0
2 3 0
```

#### Output
```sh
5
```

### Case 2

#### Input
``` sh
4
3 4 0
0 0 0
0 3 0
0 6 4
```

#### Output
``` sh
13
```

### Answer by _Sulthan Hasanal Hakim_ from _Cheetah_
``` c
#include<stdio.h>
#include<math.h>
int main() {
    int n;
    scanf("%d", &n);
    int first=0;
    int x1=0,y1=0,z1=0;
    int x2=0,y2=0,z2=0;
    int jarak = 0;
    for(int i = 1; i<n; i++) {
        if(first==0)scanf("%d %d %d", &x1, &y1, &z1);
        first=1;
        scanf("%d %d %d", &x2, &y2, &z2);
        jarak+=sqrt(((x1-x2)*(x1-x2))+((y1-y2)*(y1-y2))+((z1-y2)*(z1-z2)));
        x1=x2;
        y1=y2;
        z1=z2;
    }
    printf("%d\n", jarak);
}
```