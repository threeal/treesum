---
layout: default
---

### Problem 7: Elementary Math _(Answered)_

Input an equation, restricted to only addition(`+`) and substraction(`-`).
Each equation will always ended up with equation mark (`=`).
Every number and operator will be separated with a single space (` `).

### Case 1

#### Input
``` sh
1 + 89 =
```

#### Output
```sh
90
```

### Case 2

#### Input
``` sh
26 - 5 + 101 =
```

#### Output
``` sh
122
```

### Answer by _Androsa Kristabel_ from _Pangolin_

``` c
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
int main(){
	char str[1000];
	int sum=0,sign=1;

	printf("Make an equation\n");

	while(scanf("%s",&str)!=EOF){
		if(strcmp(str,"=")==0)break;
		else if(strcmp(str,"+")==0){
			sign=1;
		}
		else if(strcmp(str,"-")==0){
			sign=-1;
		}
		else {
			sum+=sign*atoi(str);
		}
	}

	printf("%d\n",sum);
  return 0;
}
```